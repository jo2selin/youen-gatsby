const path = require(`path`)
const locales = require("./config/i18n")
const {
  replaceTrailing,
  localizedSlug,
  replaceBoth,
  wrapper,
} = require("./src/utils/gatsby-node-helpers")

// Take the pages from src/pages and generate pages for all locales, e.g. /index and /en/index
exports.onCreatePage = ({ page, actions }) => {
  const { createPage, deletePage } = actions

  // Only create one 404 page at /404.html
  if (page.path.includes("404")) {
    return
  }

  // First delete the pages so we can re-create them
  deletePage(page)

  Object.keys(locales).map(lang => {
    // Remove the trailing slash from the path, e.g. --> /categories
    page.path = replaceTrailing(page.path)
    // Remove the leading AND traling slash from path, e.g. --> categories
    const name = replaceBoth(page.path)
    // Create the "slugs" for the pages. Unless default language, add prefix àla "/en"
    const localizedPath = locales[lang].default
      ? page.path
      : `${locales[lang].path}${page.path}`

    return createPage({
      ...page,
      path: localizedPath,
      context: {
        locale: lang,
        name,
      },
    })
  })
}

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const aboutPage = path.resolve(`src/templates/about.js`)
  const worksPage = path.resolve(`src/templates/works.js`)
  const blogPage = path.resolve(`src/templates/blog.js`)
  const caseStudy = path.resolve(`src/templates/casestudy.js`)
  const medallo = path.resolve(`src/templates/medallo.js`)
  const medallo_sub_cms = path.resolve(`src/templates/medallo_sub_cms.js`)
  return graphql(
    `
      {
        allPrismicAboutpage {
          edges {
            node {
              uid
              lang
              alternate_languages {
                uid
                lang
              }
            }
          }
        }
        allPrismicJobpage {
          edges {
            node {
              uid
              lang
              alternate_languages {
                uid
                lang
              }
            }
          }
        }
        allPrismicBlogpost {
          edges {
            node {
              lang
              uid
              alternate_languages {
                uid
                lang
              }
            }
          }
        }
        allPrismicCasestudy {
          edges {
            node {
              uid
              lang
              alternate_languages {
                uid
                lang
              }
            }
          }
        }
        allPrismicMedallopage {
          edges {
            node {
              uid
              lang
              alternate_languages {
                uid
                lang
              }
            }
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }

    // Create all multilang about pages.
    result.data.allPrismicAboutpage.edges.forEach(edge => {
      createPage({
        path: `${localizedSlug(edge.node)}`,
        component: aboutPage,
        context: {
          locale: edge.node.lang,
          uid: edge.node.uid,
          alternateLanguages: edge.node.alternate_languages,
        },
      })
    })
    // Create all multilang jobs pages.
    result.data.allPrismicJobpage.edges.forEach(edge => {
      createPage({
        path: `${localizedSlug(edge.node)}`,
        component: worksPage,
        context: {
          locale: edge.node.lang,
          uid: edge.node.uid,
          alternateLanguages: edge.node.alternate_languages,
        },
      })
    })
    // Create all multilang blog pages.
    result.data.allPrismicBlogpost.edges.forEach(edge => {
      createPage({
        path: `${localizedSlug(edge.node, "blog")}`,
        component: blogPage,
        context: {
          locale: edge.node.lang,
          uid: edge.node.uid,
        },
      })
    })
    // Create all multilang USECASES pages.
    result.data.allPrismicCasestudy.edges.forEach(edge => {
      createPage({
        path: `${localizedSlug(edge.node)}`,
        component: caseStudy,
        context: {
          locale: edge.node.lang,
          uid: edge.node.uid,
        },
      })
    })
    // Create all multilang MEDALLO pages.
    result.data.allPrismicMedallopage.edges.forEach(edge => {
      createPage({
        path: `${localizedSlug(edge.node)}`,
        component: medallo,
        context: {
          locale: edge.node.lang,
          uid: edge.node.uid,
          isDark: true,
          alternateLanguages: edge.node.alternate_languages,
        },
      })
      createPage({
        path: `${localizedSlug({ lang: edge.node.lang, uid: "starter/cms" })}`,
        component: medallo_sub_cms,
        context: {
          locale: edge.node.lang,
          uid: edge.node.uid,
          isDark: true,
          alternateLanguages: edge.node.alternate_languages,
        },
      })
    })
  })
}
