import { graphql } from "gatsby"

// export const markdownFrontmatterFragment = graphql`
//   fragment MarkdownFrontmatter on MarkdownRemark {
//     frontmatter {
//       path
//       title
//       date(formatString: "MMMM DD, YYYY")
//     }
//   }
// `

export const query = graphql`
  fragment PrismicGlobalFragment on PrismicGlobal {
    lang
    data {
      back_to_home
      contact
      read_blogpost
      use_case
      form_email_title
      form_email_placeholder
      form_email_sent
    }
  }
`
