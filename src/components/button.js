import PropTypes from "prop-types"
import React from "react"
import { css } from "@emotion/react"
import { RichText } from "prismic-reactjs"
import { colors } from "../styles/styles"

const Button = ({ children, buttonProps }) => {
  const { inverted = false, icons = false, color = "primary" } = buttonProps

  return (
    <div
      className={`
        button is-large 
        is-rounded 
        ${icons && "hasIcons"}
        ${inverted && "is-inverted"}
        ${color && `is-${color}`}
        `}
      css={buttonCss}
    >
      {icons && (
        <div className="buttonIcons">
          {icons.map((icon, i) => {
            return (
              <div key={i} className="buttonIcon">
                <span>{icon}</span>
              </div>
            )
          })}
        </div>
      )}
      <div className="buttonText">{RichText.asText(children)}</div>
    </div>
  )
}

Button.propTypes = {
  buttonProps: PropTypes.object,
}

export default Button

const buttonCss = css`
  &.button {
    font-family: inherit;
    border: none;
    box-shadow: 0px 20px 0px -10px rgb(82, 35, 128);
    padding: 0 3rem;
    &.is-primary {
      background: ${colors.white};
      &:hover {
        background: ${colors.lightgrey};
        box-shadow: 0px 17px 0px -10px rgb(82, 35, 128);
        transition-duration: 0.1s;
        transform: translateY(3px);
      }
    }
    &.hasIcons {
      .buttonIcons {
        width: 50px;
        .buttonIcon {
          position: absolute;
          top: 0;
          left: 0;
          span {
            position: absolute;
            animation: 1s ease-in-out alternate infinite wave_icons;
          }
          &:first-of-type {
            font-size: 1.6rem;
            margin: 15px 0 0 30px;
            transform: rotate(-12deg);
            span {
              animation-delay: 0.6;
              animation-duration: 0.6s;
            }
          }
          &:nth-of-type(2) {
            font-size: 1.3rem;
            margin: -15px 0 0 50px;
            transform: rotate(20deg);
            animation-delay: 5s;
            span {
              animation-delay: 0.4s;
              animation-duration: 0.9s;
            }
          }
          &:nth-of-type(3) {
            font-size: 1.1rem;
            margin: -12px 0 0 10px;
            transform: rotate(-12deg);
          }
        }
      }
    }
  }
  @keyframes wave_icons {
    0% {
      transform: translateY(0px);
    }

    100% {
      transform: translateY(-5px);
    }
  }
`
