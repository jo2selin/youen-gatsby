import Seo from "./SEO"
import Twitter from "./Twitter"
import Facebook from "./Facebook"

export { Facebook, Twitter }

export default Seo
