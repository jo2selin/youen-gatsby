const colors = require("tailwindcss/colors")

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],

  theme: {
    fontFamily: {
      sans: ["work sans"],
      serif: ["volkhov"],
    },
    colors: {
      transparent: "transparent",
      current: "currentColor",
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      polar: "#F0F8FC",
      linen: "#FCF3EF",
      oldRose: "#BF8870",
      danube: "#63A8CA",
      mulberry: "#C55677",
      comet: "#545677",
      medallo: {
        purpleDark: "#240053",
        purpleLight: "#410F83",
        salmon: "#f4967f",
      },
    },
    extend: {
      minHeight: {
        "400": "400px",
        "500": "500px",
        "600": "600px",
      },
      transform: ["hover"],
      backgroundImage: {
        "gradient-linen":
          "linear-gradient(180deg, #fcf3ef 0%, rgba(252, 243, 239, 0) 100%)",
      },
    },
  },
  plugins: [],
}
