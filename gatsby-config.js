require("dotenv").config({
  path: `.env`,
})

const prismicLinkResolver = require("./src/utils/linkResolver")

const website = require("./config/website")

const pathPrefix = website.pathPrefix === "/" ? "" : website.pathPrefix

module.exports = {
  pathPrefix: website.pathPrefix,
  siteMetadata: {
    siteUrl: website.url + pathPrefix, // For gatsby-plugin-sitemap
    pathPrefix,
    banner: website.logo,
    ogLanguage: website.ogLanguage,
    author: website.author,
    twitter: website.twitter,
    facebook: website.facebook,

    title: website.title,
    description: website.description,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${pathPrefix}static/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-image`,
    `gatsby-plugin-webpack-bundle-analyser-v2`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: website.title,
        short_name: website.titleAlt,
        description: website.description,
        start_url: website.pathPrefix,
        background_color: website.backgroundColor,
        theme_color: website.themeColor,
        display: `minimal-ui`,
        icon: website.favicon, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `volkhov\:400`,
          `work sans\:700,400,200`, // you can also specify font weights and styles
        ],
        display: "swap",
      },
    },

    {
      resolve: "gatsby-source-prismic",
      options: {
        repositoryName: "youen-gatsby",
        accessToken: `${process.env.API_KEY}`,
        linkResolver: ({ node, key, value }) => doc => {
          if (doc.type === "blogpost") return prismicLinkResolver(doc, "blog")
          // if (doc.type === "casestudy") return doc.lang + "/" + doc.uid
          return prismicLinkResolver(doc)
        },
        schemas: {
          homepage: require("./src/schemas/homepage.json"),
          menu: require("./src/schemas/menu.json"),
          global: require("./src/schemas/global.json"),
          footer: require("./src/schemas/footer.json"),
          aboutpage: require("./src/schemas/aboutpage.json"),
          blogpost: require("./src/schemas/blogpost.json"),
          casestudy: require("./src/schemas/casestudy.json"),
          medallopage: require("./src/schemas/medallopage.json"),
          casestudy2: {},
          jobpage: require("./src/schemas/jobpage.json"),
        },
        lang: "*",
      },
    },
    {
      resolve: `gatsby-plugin-emotion`,
    },
    {
      resolve: "gatsby-plugin-web-font-loader",
      options: {
        google: {
          families: ["Playfair Display"],
        },
      },
    },

    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds: [website.googleAnalyticsID],
        // This object gets passed directly to the gtag config command
        // This config will be shared across all trackingIds
        gtagConfig: {
          optimize_id: website.googleTagManager,
          anonymize_ip: true,
          cookie_expires: 0,
        },
        // This object is used for configuration specific to this plugin
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: false,
          // Setting this parameter is also optional
          respectDNT: true,
          // Avoids sending pageview hits from custom paths
          exclude: ["/preview/**"],
        },
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
